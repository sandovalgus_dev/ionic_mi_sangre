// import { HttpClient } from '@angular/common/http';
import { Http, Headers,  Response, ResponseOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';/*promise*/
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';


@Injectable()
export class ApirorProvider {
  // path : string = 'https://ionic-ror.herokuapp.com/api/';
  path : string = 'http://localhost:3000/api/';
 token : string ;

  constructor(
              public http: Http,
              public storage: Storage
              ) {
    console.log('Hello ApirorProvider Provider');

  }

// -------estado para poder donar
        consultar_si_dono(parans, tokenin){
          console.log('--- test');
          // console.log(user_id);
            this.token = 'Bearer '+ tokenin;
           let headers = new Headers();
            headers.append('Content-Type' , 'application/json');
            headers.append('Accept', 'application/json');
            headers.append('Authorization', 'Bearer ' + this.token);


              return this.http.post(`${this.path}donations/se_presento`, parans, { headers: headers }).map(
                    (res:Response) => {return res.json();}
              );
        }


// ---update profile

        updateProfile(datos, id, tokenin){
          this.token = 'Bearer '+ tokenin;
          let headers = new Headers();
            headers.append('Content-Type' , 'application/json');
            headers.append('Accept', 'application/json');
            headers.append('Authorization', 'Bearer ' + this.token);
         
            return this.http.put(`${this.path}profiles/${id}`, datos,  { headers: headers })
            .map(res => res.json(),
              err=>{
               console.log(err);
               }
              )
            // .toPromise();
            }


  // load perfil

        getProfile(user_id, tokenin){
          this.token = 'Bearer '+ tokenin;
          let headers = new Headers();
            headers.append('Content-Type' , 'application/json');
            headers.append('Accept', 'application/json');
            headers.append('Authorization', 'Bearer ' + this.token);
         
            return this.http.get(`${this.path}profile/${user_id}`,  { headers: headers })
            .map(res => res.json(),
              err=>{
               console.log(err);
               }
              )
            .toPromise();
            }

  // -************quiero donar

        quieroDonar(parans, tokenin){
            this.token = 'Bearer '+ tokenin;
           let headers = new Headers();
            headers.append('Content-Type' , 'application/json');
            headers.append('Accept', 'application/json');
            headers.append('Authorization', 'Bearer ' + this.token);


              return this.http.post(`${this.path}donations`, parans, { headers: headers }).map(
                    (res:Response) => {return res.json();}
              );
        }


// ************registo de usuario

        register(data){
            let headers = new Headers({ 'Content-Type' : 'application/json' });
                  return this.http.post(`${this.path}auth/register`, JSON.stringify(data), {
                        headers:headers,
                        method:"POST"
                   }).map(
                        (res:Response) => { 
                                  // console.log(res.access_token);
                                  return res.json();
                                   }
                  );
           }

// ************ LOGIN 
        login(data){
          let headers = new Headers({ 'Content-Type' : 'application/json' });
          return this.http.post(`${this.path}auth/login`, JSON.stringify(data), {
              headers:headers,
              method:"POST"
            }).map(
                (res:Response) => {

                            return res.json(); 
                          }
            );
          }



// ********** crear campana
        postCampana(parans, tokenin){

          this.token = 'Bearer '+ tokenin;
         let headers = new Headers();
          headers.append('Content-Type' , 'application/json');
          headers.append('Accept', 'application/json');
          headers.append('Authorization', 'Bearer ' + this.token);


            return this.http.post(`${this.path}campaigns`, parans, { headers: headers }).map(
                  (res:Response) => {return res.json();}
            );
          }

// ************* traer todos los hospitales

        getHospitals(){
          let headers: Headers = new Headers({ 'Content-Type': 'application/json' });

          return this.http.get(`${this.path}hospitals/`,  { headers: headers }).map(res => res.json(),
          err => {
             console.log(err);
             }
            )
            .toPromise();
          }

// ************traer un solo hospital

        getHospital(id: number) {
          let headers: Headers = new Headers({ 
            'Authorization': 'Bearer'+'eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE1MTgyMTIyNDV9.kOGotCoQnQRd0sq3mfc2RkNDm8s8094m0PeBMt6en18',
                              'Content-Type': 'application/json' });
       
          return this.http.get(`${this.path}hospitals/${id}`,  { headers: headers })
          .map(res => res.json(),
          err=>{
           console.log(err);
           }
          )
          .toPromise();
        }

// ***********traer todas las  campanas

        getCampaigns(){
            return this.http.get(`${this.path}campaigns/`).map(res => res.json(),
            err => {
             console.log(err);
             }
            )
            .toPromise();
          }

// *********traer una campana

        getCampaign(id: number) {
          return this.http.get(`${this.path}campaigns/${id}`)
          .map(res => res.json(),
          err=>{
             console.log(err);
            }
          )
          .toPromise();
        }


}
