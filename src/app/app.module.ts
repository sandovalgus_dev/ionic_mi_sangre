import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';

import {IoniciotabsPage} from '../pages/ioniciotabs/ioniciotabs';
import {HospitalesPage} from '../pages/hospitales/hospitales';
import {NuevaCampanaPage} from '../pages/nueva-campana/nueva-campana';
import {MisCampanasPage} from '../pages/mis-campanas/mis-campanas';
import { LoginPage } from '../pages/login/login';
import { SingupPage } from '../pages/singup/singup';
import { PerfilUsuarioPage } from '../pages/perfil-usuario/perfil-usuario';

// import {CampanaDetallePage} from '../pages/campana-detalle/campana-detalle';

import { GoogleMaps } from '@ionic-native/google-maps';

import { Geolocation } from '@ionic-native/geolocation';

import { ApirorProvider } from '../providers/apiror';


// import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { IonicStorageModule } from '@ionic/storage';


 
@NgModule({
  declarations: [
    MyApp,
    IoniciotabsPage,
    HospitalesPage,
    NuevaCampanaPage,
    MisCampanasPage,
    LoginPage,
    SingupPage,
    PerfilUsuarioPage
    // CampanaDetallePage
    // HospitalDetailsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicStorageModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    IoniciotabsPage,
    HospitalesPage,
    NuevaCampanaPage,
    MisCampanasPage,
    LoginPage,
    SingupPage,
    PerfilUsuarioPage
    // CampanaDetallePage
        // HospitalDetailsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    GoogleMaps,
    Geolocation,
    LaunchNavigator,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApirorProvider
  ]
})
export class AppModule {}
