import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,ViewController } from 'ionic-angular';
 import { Storage } from '@ionic/storage';
// import { NativeStorage } from '@ionic-native/native-storage';
import { ApirorProvider } from '../../providers/apiror';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { IoniciotabsPage } from '../ioniciotabs/ioniciotabs';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */ 



@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
	ingreso: FormGroup;
	  constructor(
	  				public navCtrl: NavController, 
	  				public navParams: NavParams,
					private viewCtrl : ViewController,
					public http: ApirorProvider,
					public storage: Storage,
					public formBuilder: FormBuilder,
					public alertCtrl: AlertController) {
	  
	  				this.ingreso = this.createMyForm();
	  				}

	 ionViewDidLoad() {
	    console.log('ionViewDidLoad LoginPage');
	  }

	private createMyForm(){
			return this.formBuilder.group({
				email: ['', Validators.required],
				password: ['', Validators.required],
			});
		} 

	signin(){
		console.log(this.ingreso.value);
		// this.navCtrl.push(TabsPage);
		this.http.login(this.ingreso.value)
		.subscribe(
			data => {
				this.storage.set('token', data.access_token);
				console.log('token ---> '+ data.access_token);
				this.storage.set('user_id', data.id);
				this.navCtrl.push(IoniciotabsPage);
				// console.log('-----------token-------------');
				// console.log(data.access_token);
				// console.log('--------------token------------- ');

				// this.storage.get('token').then((val) => {
				//  this.showAlert(val);
				// });

			},
			err=> {console.log(err);}
			);
		}

	showAlert(men){
		let alert = this.alertCtrl.create({
				title: 'Informacion',
				subTitle: men,
				buttons: ['ok']
			});
			alert.present();
		} 


}
 