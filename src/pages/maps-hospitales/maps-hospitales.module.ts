import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MapsHospitalesPage } from './maps-hospitales';

@NgModule({
  declarations: [
    MapsHospitalesPage,
  ],
  imports: [
    IonicPageModule.forChild(MapsHospitalesPage),
  ],
})
export class MapsHospitalesPageModule {}
