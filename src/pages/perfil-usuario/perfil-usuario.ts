import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , ViewController, ModalController} from 'ionic-angular';
import { ApirorProvider } from '../../providers/apiror';
import { Storage } from '@ionic/storage';
import { PerfilEditarPage } from '../perfil-editar/perfil-editar';

/**
 * Generated class for the PerfilUsuarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */ 
  
@IonicPage()
@Component({
  selector: 'page-perfil-usuario',
  templateUrl: 'perfil-usuario.html',
})
export class PerfilUsuarioPage {
		id_user: any;
		token: string;
		perfil: any;

	  constructor(
		  	public navCtrl: NavController, 
		  	public navParams: NavParams,
		  	private viewCtrl : ViewController,
  			public http: ApirorProvider,
  			public storage: Storage,
  			public modalCtrl : ModalController)
						{
								this.storage.get('token').then((val) => {
									this.token = val;
									 console.log('id del usuario is --> '+ this.token);
							
								});
								this.storage.get('user_id').then((val) => {
									this.id_user = val;
									 console.log('id del usuario is --> '+ this.id_user);
							
								});


						}

		  ionViewWillEnter() 
		
		  {
		    // console.log('ionViewDidLoad PerfilUsuarioPage');
		     this.loadPerfil();
		  }


	  	  loadPerfil(){

			  		console.log('-----id user---');
			  		console.log(this.id_user);
			  		console.log('-----token---');
			  		console.log(this.token);

					this.http.getProfile(this.id_user, this.token).then( res => {		
						console.log(res);
						 this.perfil = res.profile;  

				    },
				    error =>{
				      console.log(error);
				    });

				}

		vistaEditar(){
			let id_user = this.id_user;
			console.log(this.id_user);
			 let mimodal = this.modalCtrl.create( 'PerfilEditarPage', {id_user});
  	  		 mimodal.present();
		}
}


