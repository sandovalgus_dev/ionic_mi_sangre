import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,  ViewController, AlertController	 } from 'ionic-angular';
import { ApirorProvider } from '../../providers/apiror';
import { Storage } from '@ionic/storage';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import {PerfilUsuarioPage} from '../perfil-usuario/perfil-usuario';

/**
 * Generated class for the PerfilEditarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */     
@IonicPage()
@Component({
  selector: 'page-perfil-editar',
  templateUrl: 'perfil-editar.html',
})
export class PerfilEditarPage {
	// perfil: any;
	perfil: FormGroup;
	profile: any;
	token: string;
	id_user: any;
	id_profile: any;
  		constructor
  				(public navCtrl: NavController, 
  				public navParams: NavParams,
  				private viewCtrl : ViewController,
  				public http: ApirorProvider,
  				public formBuilder: FormBuilder,
  				public storage: Storage,
  				public alertCtrl: AlertController) 
  								{
  					
					

					this.id_user = navParams.get('id_user');
					this.storage.get('token').then((val) => {
						this.token = val;
						

						});

					 this.perfil = this.createMyForm();
					  
 								 }


			private createMyForm(){
					return this.formBuilder.group({

					firs_name: ['', Validators],
					last_name: ['', Validators],
					bloode_type: ['', Validators],
					bio: ['', Validators],
					location: ['', Validators],



					});
				}

		enviar(){


				this.http.updateProfile(this.perfil.value, this.id_profile,  this.token)
				.subscribe(
					data => {console.log(data);
						 this.showAlert();
						 this.cerrarModal();
						 this.navCtrl.setRoot(PerfilUsuarioPage);
						// this.getDados();      
 					},
					err=> {console.log(err);}
				);


		}

	  ionViewDidLoad() {
		     this.loadPerfil();

		  }

		cerrarModal(){
			this.viewCtrl.dismiss();
			}

	  	  loadPerfil(){

		  		
				this.http.getProfile(this.id_user, this.token).then(
				 res => {		
				 			this.profile = res.profile;
			
				 			this.perfil.get('firs_name').setValue(this.profile.firs_name);
				 			this.perfil.get('last_name').setValue(this.profile.last_name);
				 			this.perfil.get('bloode_type').setValue(this.profile.bloode_type);
				 			this.perfil.get('bio').setValue(this.profile.bio);
				 			this.perfil.get('location').setValue(this.profile.location);
							this.id_profile = this.profile.id;
					// asignacion de datos 


			    },
			    error =>{
			      console.log(error);
			    });

			}


			    showAlert(){
			    	let alert = this.alertCtrl.create({
			    		title: 'Informacion',
			    		subTitle: 'Los datos han sido actualizados',
			    		buttons: ['ok']
			    	});
			    	alert.present();
			    }




}
