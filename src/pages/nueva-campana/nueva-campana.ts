import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ApirorProvider } from '../../providers/apiror';
import {MisCampanasPage} from '../mis-campanas/mis-campanas';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the NuevaCampanaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */   

@IonicPage()
@Component({
  selector: 'page-nueva-campana',
  templateUrl: 'nueva-campana.html',
})
export class NuevaCampanaPage {
	// cadastro : any = {};
	cadastro: FormGroup;
	hospitals: any[];
	id_user: number;
	token: string;

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	public formBuilder: FormBuilder,
  	public http: ApirorProvider,
  	public storage: Storage,
  	public alertCtrl: AlertController) {
  

  	 this.cadastro = this.createMyForm();
  }

			ionViewDidLoad() {

				    this.http.getHospitals().then( res => {
					
						console.log(res.hospitals);
						this.hospitals = res.hospitals;
						
						this.storage.get('user_id').then((val) => {
						this.id_user = val;
					
						});
						this.storage.get('token').then((val) => {
						this.token = val;
					
						});
			          // console.log(this.hospitals)
			        },
			        error =>{
			          console.log(error);
			        })

				}





		private createMyForm(){
		  return this.formBuilder.group({

		    patient: ['', Validators.required],
		    sex:['M',[Validators.required]],
		    type_of_donation: ['', Validators.required],
		    bloode_type: ['', Validators.required],
		    birthday: ['', Validators.required],
		    number_of_donors: ['', Validators.required],
		    city: ['', Validators.required],
		    comment: ['', Validators.required],
		    hospital_id: ['', Validators.required],
		  	user_id: ['', Validators],


		  });
		}




		      enviar() {
				console.log('----patient------');
				this.cadastro.value.user_id = this.id_user;
		
				console.log('token is friends? '+ this.token);

	            this.http.postCampana(this.cadastro.value, this.token)
	                  .subscribe(
	                        data => {console.log(data);
	                        		this.showAlert('excelente');
	                        		this.navCtrl.setRoot(MisCampanasPage);
	                              // this.getDados();      
	                        },
	                        err=> {console.log(err);}
	                  );
			      }

			    showAlert(men){
			    	let alert = this.alertCtrl.create({
			    		title: 'Informacion',
			    		subTitle: 'excelente',
			    		buttons: ['ok']
			    	});
			    	alert.present();
			    }

			     traerHospitales(){

				    this.http.getHospitals().then( res => {
				          console.log(res.hospitals);
				           this.hospitals = res.hospitals;
				          // console.log(this.hospitals)
				        },
				        error =>{
				          console.log(error);
				        });
				   }




}
