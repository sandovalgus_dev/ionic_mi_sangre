import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NuevaCampanaPage } from './nueva-campana';

@NgModule({
  declarations: [
    NuevaCampanaPage,
  ],
  imports: [
    IonicPageModule.forChild(NuevaCampanaPage),
  ],
})
export class NuevaCampanaPageModule {}
