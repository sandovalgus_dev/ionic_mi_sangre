import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MisCampanasPage } from './mis-campanas';

@NgModule({
  declarations: [
    MisCampanasPage,
  ],
  imports: [
    IonicPageModule.forChild(MisCampanasPage),
  ],
})
export class MisCampanasPageModule {}
