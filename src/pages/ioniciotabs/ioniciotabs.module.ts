import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IoniciotabsPage } from './ioniciotabs';

@NgModule({
  declarations: [
    IoniciotabsPage,
  ],
  imports: [
    IonicPageModule.forChild(IoniciotabsPage),
  ]
})
export class IoniciotabsPageModule {}
