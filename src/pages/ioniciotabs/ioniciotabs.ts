import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

/**
 * Generated class for the IoniciotabsPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ioniciotabs',
  templateUrl: 'ioniciotabs.html'
})
export class IoniciotabsPage {

  generalStartRoot = 'GeneralStartPage'
  personalStartRoot = 'PersonalStartPage'


  constructor(public navCtrl: NavController) {}

}
