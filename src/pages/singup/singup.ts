import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ApirorProvider } from '../../providers/apiror';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
/**
 * Generated class for the SingupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-singup',
  templateUrl: 'singup.html',
})
export class SingupPage {
	register: FormGroup;
	  constructor(
	  				public navCtrl: NavController, 
	  				public navParams: NavParams,
					private viewCtrl : ViewController,
					public http: ApirorProvider,
					public storage: Storage,
					public formBuilder: FormBuilder,
					public alertCtrl: AlertController) {
	  
	  				this.register = this.createMyForm();
	  				}

		  ionViewDidLoad() {
		    console.log('ionViewDidLoad SingupPage');
		  }

		private createMyForm(){
		      return this.formBuilder.group({

		          name: ['', Validators.required],
		          email: ['', Validators.required],
		          password: ['', Validators.required],
		          
		        });
		      }


		 signup() {
		      console.log(this.register.value.name);
		        // this.storage.set('name', this.register.value.name);
		      // this.navCtrl.push(TabsPage);
		          this.http.register(this.register.value)
		                .subscribe(
		                      data => {console.log(data);
		                        
		                      },
		                      err=> {console.log(err);}
		                );
		  }

}
 