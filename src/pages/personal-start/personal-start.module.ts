import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PersonalStartPage } from './personal-start';

@NgModule({
  declarations: [
    PersonalStartPage,
  ],
  imports: [
    IonicPageModule.forChild(PersonalStartPage),
  ],
})
export class PersonalStartPageModule {}
