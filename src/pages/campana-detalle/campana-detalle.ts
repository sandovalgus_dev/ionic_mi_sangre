import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , ViewController} from 'ionic-angular';
import { ApirorProvider } from '../../providers/apiror';
import { Storage } from '@ionic/storage';
 
@IonicPage() 
@Component({
  selector: 'page-campana-detalle',
  templateUrl: 'campana-detalle.html',
})
export class CampanaDetallePage {

	id_campana: any;
	campaign: any;
	id_user: any;
	token: string;
 	donar = {"user_id": "","campaign_id": ""};
 	isenabled:boolean=false;
 	dono = {"user_id": ""};
 
 
  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	private viewCtrl : ViewController,
  	public http: ApirorProvider,
  	public storage: Storage) {
 				
		 	 	this.id_campana = navParams.get('id');
				
				this.storage.get('user_id').then((val) => {
					this.id_user = val;
			
				});
				this.storage.get('token').then((val) => {
					this.token = val;
			
				});
  			}

	  ionViewDidEnter() {  

			this.consultar_dono();
			 this.loadCampana();

		  }

      goBack():void{
		  	this.navCtrl.pop();

		  	// .pop regresa a la pila anterior
		  }

		loadCampana(){
	        this.http.getCampaign(this.id_campana).then( res => {
	      console.log(res.campaign);
	       this.campaign = res.campaign;
	       console.log('campana id is --> '+ this.campaign.id);
	    
		    },
		    error =>{
		      console.log(error);
		    });
		}

		cerrarModal(){
			this.viewCtrl.dismiss();
			}

		quiero_donar(){

			this.donar.user_id = this.id_user;
			this.donar.campaign_id = this.id_campana;
			// console.log('this donar user ---> '+ this.donar.id_user );
			// console.log('this donar campana---> '+ this.donar.id_campana );
				this.http.quieroDonar(this.donar, this.token)
				.subscribe(
					data => {console.log(data);
					// this.showAlert('excelente');
					// this.navCtrl.setRoot(MisCampanasPage);
					// this.getDados();      
					},
					err=> {console.log(err);}
				);

			}

			consultar_dono(){
				console.log('--- user id --> '+ this.id_user);
				// let id = this.id_user;
				this.dono.user_id = this.id_user
				this.http.consultar_si_dono(this.dono, this.token)
				.subscribe(
					data => {console.log(data);
						this.isenabled = data;
						console.log('vos ya donaste?');
	     
					},
					err=> {console.log(err);}
				);

			}

}
