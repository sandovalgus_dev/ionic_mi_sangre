import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CampanaDetallePage } from './campana-detalle';

@NgModule({
  declarations: [
    CampanaDetallePage,
  ],
  imports: [
    IonicPageModule.forChild(CampanaDetallePage),
  ],
})
export class CampanaDetallePageModule {}
