import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListHospitalesPage } from './list-hospitales';

@NgModule({
  declarations: [
    ListHospitalesPage,
  ],
  imports: [
    IonicPageModule.forChild(ListHospitalesPage),
  ],
})
export class ListHospitalesPageModule {}
