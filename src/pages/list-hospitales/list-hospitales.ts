import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { HospitalDetailsPage } from '../hospital-details/hospital-details';
import { ApirorProvider } from '../../providers/apiror';
/**
 * Generated class for the ListHospitalesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */ 

@IonicPage()
@Component({
  selector: 'page-list-hospitales',
  templateUrl: 'list-hospitales.html',
})
export class ListHospitalesPage {
hospitals: any[];
  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	public modalCtrl : ModalController,
    public http : ApirorProvider) {
  }
  

    ionViewDidEnter(){

      // promesa
    this.http.getHospitals().then( res => {
          console.log(res.hospitals);
           this.hospitals = res.hospitals;
          // console.log(this.hospitals)
        },
        error =>{
          console.log(error);
        });
   }

    goToDetails(id: number) {
    this.navCtrl.push(HospitalDetailsPage, {id});
  }

 
  	  nuevoSitio(id: number){
  	  // aquí vamos a abrir el modal para añadir nuestro sitio.
  	   let mimodal = this.modalCtrl.create( 'HospitalDetailsPage',  {id});
  	   mimodal.present();
  	}
	}
