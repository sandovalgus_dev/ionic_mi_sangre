import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GeneralStartPage } from './general-start';

@NgModule({
  declarations: [
    GeneralStartPage,
  ],
  imports: [
    IonicPageModule.forChild(GeneralStartPage),
  ],
})
export class GeneralStartPageModule {}
