import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { ApirorProvider } from '../../providers/apiror';
// import {CampanaDetallePage} from '../campana-detalle/campana-detalle';


@IonicPage()
@Component({
  selector: 'page-general-start',
  templateUrl: 'general-start.html',
})
export class GeneralStartPage {
	campanas: any[];


  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
    public modalCtrl : ModalController,
  	public http: ApirorProvider) {

  }

  ionViewDidLoad() {
    

          // promesa
        this.http.getCampaigns().then( res => {
              console.log(res.campaigns);
               this.campanas = res.campaigns;
              // console.log(this.hospitals)
            },
            error =>{
              console.log(error);
            });
      }

        // goCampana_detalle(id: number) {
        //   this.navCtrl.push('CampanaDetallePage', {id});
        // }

        goCampana_detalle(id: number){
          // aquí vamos a abrir el modal para añadir nuestro sitio.
           let mimodal = this.modalCtrl.create( 'CampanaDetallePage',  {id});
           mimodal.present();
        }

}
