import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Platform	 } from 'ionic-angular';
import { ApirorProvider } from '../../providers/apiror';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker
} from '@ionic-native/google-maps';

import { Geolocation } from '@ionic-native/geolocation';
import { LaunchNavigator } from '@ionic-native/launch-navigator';


declare var google: any;

 
@IonicPage()
@Component({
  selector: 'page-hospital-details',
  templateUrl: 'hospital-details.html',
})
export class HospitalDetailsPage {
  map: any; // Manejador del mapa
  coords : any = { lat: 0, lng: 0 }
  id: number;
  name: string;
  hospital: any;
  lat:number;
  lng:number;
  // map: GoogleMap;
 
 
  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	private viewCtrl : ViewController,
    private http : ApirorProvider,
    private googleMaps: GoogleMaps,
     public  platform: Platform,
   private geolocation: Geolocation,
    private launchNavigator: LaunchNavigator) {

      this.id = navParams.get('id');


      http.getHospital(this.id).then(res => {

        this.hospital = res.hospital;
 
        console.log("this.hospital.latitude");
        console.log(this.hospital.latitude);
           this.coords.lat = this.hospital.latitude;
        console.log("this.coords.lat");
        console.log(  this.coords.lat);
     

        this.coords.lng = this.hospital.longitude;

        let mapContainer = document.getElementById('map');
          this.map = new google.maps.Map(mapContainer, {
            center: this.coords,
            zoom: 12
          });
        // Colocamos el marcador
        //marcador yo
        let miMarker = new google.maps.Marker({
          icon : 'assets/imgs/ico_estoy_aqui.png',
          map: this.map,
          position: this.coords
        }); 

        // this.loadMap();
         // this.loadMap();
        console.log(this.hospital);
    })
      
  }

  

  ionViewDidLoad() {

    console.log('ionViewDidLoad HospitalDetailsPage');
  }

    cerrarModal(){
      this.viewCtrl.dismiss();
    }


    comoLlegar(){
      let destino = this.hospital.latitude +', '+this.hospital.longitude;
      this.launchNavigator.navigate(destino)
      .then(
        success => console.log('Launched navigator'),
        error => console.log('Error launching navigator', error)
      );
      
   }






}
