import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

/**
 * Generated class for the HospitalesPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-hospitales',
  templateUrl: 'hospitales.html'
})
export class HospitalesPage {

  listHospitalesRoot = 'ListHospitalesPage'
  mapsHospitalesRoot = 'MapsHospitalesPage'


  constructor(public navCtrl: NavController) {}

}
