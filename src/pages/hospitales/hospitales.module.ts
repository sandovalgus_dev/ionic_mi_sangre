import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HospitalesPage } from './hospitales';

@NgModule({
  declarations: [
    HospitalesPage,
  ],
  imports: [
    IonicPageModule.forChild(HospitalesPage),
  ]
})
export class HospitalesPageModule {}
